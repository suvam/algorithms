# README #

This repository is a collection of some interesting algorithmic problems I came across. By nature, this repository is perpetually under construction.

### What is this repository for? ###

* A collection of implementations of algorithmic problems from a variety of sources. I have tried to cite the source of the problem whenever possible, though (given the fact that some problems have become folklore) this is not always adhered to.

### How do I get set up? ###

* All you need is Java, and a decent Java IDE like Eclipse. You also need to set-up Git to clone this repo, or create branches.
* Dependencies: None yet
* Simply clone the repository using git clone (you can get the complete command from one of the buttons on the panel to the left), create a new project in Eclipse, and you're good to go.


### Who do I talk to? ###

* I would love to hear from you regarding the quality and performance of the code. Feel free to contact me at suvamm@outlook.com
Also, feel free to email me problems which you believe ought to be added to the repository for completeness purposes.