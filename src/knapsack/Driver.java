/**
 * Input: args[0]: path to input file
 * 
 * Input File Format:
 * #items
 * #maxWeight
 * Item1_weight Item1_value
 * ...
 * ItemN_weight ItemN_value
 * 
 * Output: Max value, Subset which yields max value
 */
package knapsack;

import java.io.*;
import java.util.StringTokenizer;

/**
 * @author suvam
 *
 */
public class Driver {

	public static int max(int a, int b)
	{
		if(a>b)
			return a;
		else
			return b;
	}
	
	public static void printSolution(int n, int w, int[][] M, Item[] items)
	{
		if(n!=0)
		{
			if(items[n].getWeight() > w)
			{
				printSolution(n-1, w, M, items);
			}
			else if(M[n][w] == M[n-1][w-items[n].getWeight()] + items[n].getWeight())
			{
				System.out.println(n + " with weight: " + items[n].getWeight());
				printSolution(n-1, w-items[n].getWeight(), M, items);
			}
			else
			{
				printSolution(n-1, w, M, items);
			}
		}
			
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		if(args.length != 1)
		{
			System.out.println("\nIncorrect number of arguments");
			System.exit(1);
		}
		
		// args[0] is the input file path
		BufferedReader br = new BufferedReader(new FileReader(new File(args[0])));
		
		int numOfItems = Integer.parseInt(br.readLine());	// 1st line has numIntems
		Item[] items = new Item[numOfItems+1];
		
		int maxWeight = Integer.parseInt(br.readLine());	// 2nd line has maximum weight
		
		// parse input file
		for(int i=1; i<items.length; i++)
		{
			String line = br.readLine();
			System.out.println("\nNow reading: " + line);
			StringTokenizer st = new StringTokenizer(line);
			int weight, value;
			weight = Integer.parseInt(st.nextToken());
			value = Integer.parseInt(st.nextToken());
			
			items[i] = new Item(weight, value);
		}
		
		// Build the Memoized Solution
		int[][] M = new int[numOfItems+1][maxWeight+1];
		
		for(int i=0; i<=numOfItems; i++)
			M[i][0] = 0;
		
		for(int i=0; i <= maxWeight; i++)
			M[0][i] = 0;
		
		for(int i=1; i<=numOfItems; i++)
			for(int j=1; j<=maxWeight; j++)
			{
				if(items[i].getWeight() > j)
					M[i][j] = M[i-1][j];
				else
				{
					M[i][j]= max(M[i-1][j], M[i-1][j-items[i].getWeight()] + items[i].getWeight());
				}
			}
		
		System.out.println("\nMaximum weight: " + M[numOfItems][maxWeight]);
		System.out.println("Printing out the optimal subset: ");
		
		printSolution(numOfItems, maxWeight, M, items);
		br.close();
		
	}

}

