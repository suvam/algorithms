# Child can take 1 or 2 or 3 steps. Find ways in which child covers n steps.
# Suvam Mukherjee
i = 0;
n = 1000;
memo = [0] * n;
memo[0] = 0;
memo[1] = 1;
memo[2] = 2;
memo[3] = 4;
for i in range(4, len(memo)):
  memo[i] = memo[i-1] + memo[i-2] + memo[i-3]

print memo[n-1]

