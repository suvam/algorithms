/**
 * Adds additional capabilities to a String object.
 */
package myString;

import java.util.ArrayList;

/**
 * @author suvam
 *
 */
public class ExtendedString {

	private String s;
	
	public ExtendedString(String s) {
		this.s = s;
	}
	
	/**
	 * Returns factorial of the input
	 * @param n
	 * @return n!
	 */
	protected int factorial(int n) {
		int fact = 1;
		
		if(n < 0)
			return -1;
		
		if(n==0)
			return 1;
		
		for(int i=1; i<=n; i++) {
			fact *= i;
		}
		return fact;
	}
	
	/**
	 * Return a permutation of the input string
	 * @param s : string to be permuted
	 * @return : all permutations of input string s
	 */
	public ArrayList<String> permute(String s) {
		ArrayList<String> permutations = new ArrayList<String>();
		permute(s, "", permutations);
		return permutations;
	}
	
	/**
	 * Permute the string associated with this object.
	 * @return all permutations of string associated with this object
	 */
	public ArrayList<String> permuteThis() {
		ArrayList<String> permutations = new ArrayList<String>();
		permute(this.s, "", permutations);
		return permutations;
	}
	
	/**
	 * Compute the permutations of rem, by considering a pivot, adding it to prefix, and recursively calling permutation on the remaining string.
	 * @param str
	 * @param prefix : pivot
	 * @param permutations
	 */
	public void permute(String str, String prefix, ArrayList<String> permutations) {
		if(str.length() == 0) {
			permutations.add(prefix);
		}
		else {
			for(int i =0; i<str.length(); i++) {
				// Compute the string sans the pivot element, which is i
				String rem = str.substring(0, i) + str.substring(i+1);
				// Recurse with i as pivot
				permute(rem, prefix+str.charAt(i), permutations);
			}
		}
	}
}
