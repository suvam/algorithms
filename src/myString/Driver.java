/**
 * Generic driver for various methods of ExtendedString
 */
package myString;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * @author suvam
 *
 */
public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Test permutations
		System.out.println("\nEnter string to permute: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = null;
		try {
			input = br.readLine();
			input = input.trim();
		}
		catch(IOException e) {
			System.out.println("\nAn I/O Exception occurred while reading.");
		}
		
		ExtendedString es = new ExtendedString(input);
		ArrayList<String> permutations  = es.permuteThis();
		System.out.println("\nPermuations: \n");
		for(String s: permutations) {
			System.out.println(s);
		}
	}

}
