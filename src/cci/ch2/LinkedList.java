// LinkedList library for Chapter 2 problems
// Suvam Mukherjee
package cci.ch2;

public class LinkedList {

	private Node head;
	private Node tail;

	public LinkedList() {
		head = null;
	}

	public Node getHead() {
		return this.head;
	}

	public void setHead(Node head) {
		this.head = head;
	}
	
	public void setTail(Node node) {
		this.tail = node;
	}
	
	public Node getTail() {
		return this.tail;
	}

	// add node to end of list
	public void addNode(Node inNode) {
		Node n = new Node(inNode);

		if(this.head == null) {
			head = new Node();
			head.setData(n.getData());
			tail = head;
		}
		else {
			tail.setNext(n);
			tail.getNext().setNext(null);
			tail = tail.getNext();
		}
	}

	public void addNode(int n) {
		Node node = new Node(n, null);
		this.addNode(node);
	}

	// add node at start of list
	public void addNodeAtHead(Node inNode) {
		
		if(this.head == null) {
			head = new Node();
			head.setData(inNode.getData());
			tail = head;
		}
		else {
			Node n = new Node(inNode);
			n.setNext(head);
			head = n;
		}
	}

	public void addNodeAtHead(int value) {
		Node n = new Node(value, null);
		addNodeAtHead(n);
	}

	// utility function to print out a list
	public void printList() {
		if(this.head == null)
			System.out.println("empty");
		else {
			Node current = head;
			while(current.getNext() != null) {
				System.out.print(current.getData() + " --> ");
				current  = current.getNext();
			}
			System.out.println(current.getData());
		}
	}
}
