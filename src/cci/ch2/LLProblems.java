// Linked List problems in Chapter 2, CCI
// Suvam Mukherjee
package cci.ch2;

import java.util.HashSet;
import java.io.*;

class Res {
	public Node n;	
	public boolean result;

	public Res(Node n, boolean result) {
		this.n = n;
		this.result = result;
	}
}

public class LLProblems {

	// recursive solution to check if a linked list is a palindrome
	// TODO : IMPORTANT!!!
	public static Res checkPalindromeRecurse(Node head, int length) {
			
		if(head==null || length <= 0) 	// even length list
			return new Res(head, true);
	
		if(length == 1) 	// odd length list
			return new Res(head.getNext(), true);

		Res res = checkPalindromeRecurse(head.getNext(), length-2);

		if(res.n == null || res.result == false)	// not a palindrome
			return res;

		// compare data
		res.result = (head.getData() == res.n.getData());
		// modify the res node
		res.n = res.n.getNext();

		return res;  
	}

	// check a linked list whether it is a palindrome
	public static boolean checkPalindrome(LinkedList l) {
		int size = getSize(l);
		Res r = checkPalindromeRecurse(l.getHead(), size);
		return r.result;
	}

	// reverse a linked list in place
	public static void reverse(LinkedList l) {
		if(l.getHead() == null)
			return;
		if(l.getHead().getNext() == null)
			return;

		Node a = l.getHead();
		Node b = a.getNext();
		Node c = b.getNext();
		while(c != null) {
			b.setNext(a);
			a = b;
			b = c;
			c = c.getNext();
		}
		b.setNext(a);

		Node temp = l.getTail();
		l.setTail(l.getHead());
		l.setHead(temp);
		l.getTail().setNext(null);
		
	} 	

	// return a new linked list which is reverse of old linked list
	public static LinkedList reverseAndReturn(LinkedList l) {
		LinkedList reversedL = new LinkedList();
		
		Node curr = l.getHead();

		while(curr != null) {
			Node temp = new Node(curr);
			reversedL.addNodeAtHead(temp);
			curr = curr.getNext();
		}

		return reversedL;
	}
	
	// return size of a linked list
	public static int getSize(LinkedList l) {
		int size = 0;
		Node curr = l.getHead();
	
		while(curr != null) {
			size++;
			curr = curr.getNext();
		}
		return size;
	}

	// compare two lists element wise 
	public static boolean compareLists(LinkedList l1, LinkedList l2) {
		
		if(getSize(l1) != getSize(l2)) 	
			return false;

		Node currL1 = l1.getHead();
		Node currL2 = l2.getHead();

		while(currL1 != null) {
			if(currL1.getData() != currL2.getData())
				return false;
			currL1 = currL1.getNext();
			currL2 = currL2.getNext();
		}
		return true;
	}
			
	// add two numbers represented as a list in a reverse order
	public static LinkedList sumReverse(LinkedList a, LinkedList b) {
		Node currA = a.getHead();
		Node currB = b.getHead();
		int carry = 0;
		LinkedList result = new LinkedList();
		
		while(currA!=null && currB!=null) {
			int value = (currA.getData() + currB.getData() + carry) % 10;
			result.addNode(value);
			carry = (currA.getData() + currB.getData() + carry) / 10;
			currA = currA.getNext();
			currB = currB.getNext();
		}

		if(currA==null && currB==null) {
			if(carry != 0)
				result.addNode(carry); }
		else if(currA==null && currB!=null) {
			int value = (currB.getData() + carry) % 10;
			carry = (currB.getData() + carry) / 10;
			result.addNode(value);
			if(carry != 0) 
				result.addNode(carry);
			}
		else if(currA!=null && currB==null) {
			int value = (currA.getData() + carry) % 10;
			carry = (currA.getData() + carry) / 10;
			result.addNode(value);
			if(carry != 0)
				result.addNode(carry);
			}
		return result;
	}

			
	// partition a linked list around a given value
	public static void partition(LinkedList l, int key) {
		Node current = l.getHead();
		while(current.getNext() != null) {
			if(current.getNext().getData() < key) {
				Node temp = current.getNext();
				current.setNext(temp.getNext());
				temp.setNext(l.getHead());
				l.setHead(temp);
			}
			else	
				current = current.getNext();
		}
	}


	// remove duplicates in a linked list
	public static void removeDuplicates(LinkedList l) {
		if(l.getHead() == null)
			return;

		HashSet<Integer> seen = new HashSet<Integer>();
		Node head = l.getHead();
		seen.add(head.getData());

		Node current = head;

		while(current.getNext() != null) {
			// next item has been seen before
			if(seen.contains(current.getNext().getData())) {
				current.setNext(current.getNext().getNext());
			}
			else {
				seen.add(current.getNext().getData());
				current = current.getNext();
			}
		}
	}


	public static void main(String[] args) throws IOException {
		
		String choice = "y";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("How many linked lists?: ");
		int numLists = Integer.parseInt(br.readLine().trim());
		LinkedList[] lists = new LinkedList[numLists];
		
		for(int i=0; i<numLists; i++) {
			lists[i] = new LinkedList();
			System.out.println("Set-up linked list " + (i+1));
			
			System.out.println("Enter element?[y/n]: ");
			choice = br.readLine().trim();
			
			while(choice.equalsIgnoreCase("y")) {
				System.out.println("Enter number: ");
				int value = Integer.parseInt(br.readLine());
				lists[i].addNode(value);
				System.out.println("Enter element?[y/n]: ");
				choice = br.readLine().trim();
			}
		}
		
		// Print out lists
		for(int i=0; i<numLists; i++) {
			lists[i].printList();
		}
		// reverse(lists[0]);
		// lists[0].printList();
		//LinkedList sum = sumReverse(lists[0], lists[1]);
		//sum.printList();

		//LinkedList reversed = reverseAndReturn(lists[0]);
		//System.out.println(compareLists(lists[0], reversed));
		System.out.println(checkPalindrome(lists[0]));
	}
}				
