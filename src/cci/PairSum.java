/**
 * CCI problem.
 * Given an array of distinct integers, find all pairs that have difference k
 */
package cci;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author suvam
 *
 */
public class PairSum {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		int numInt = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int[] numbers;
		int k =  0;
		HashSet<Integer> obsNum = new HashSet<Integer>();
		HashSet<Integer> sum = new HashSet<Integer>();
		int count = 0;

		System.out.println("\nEnter number of elements: ");
		numInt = Integer.parseInt(br.readLine());
		numbers = new int[numInt];
		
		for(int i =0 ; i<numInt; i++) {
			System.out.print("\nEnter number " + (i+1) + ": ");
			numbers[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("\nEnter k: ");
		k = Integer.parseInt(br.readLine());		

		for(int i =0; i<numbers.length; i++) {
			obsNum.add(numbers[i]);
		}

		for(int i =0; i<numbers.length; i++) {
			if(obsNum.contains(numbers[i] + k)) {
				// Avoid duplicates
				if(! sum.contains(numbers[i] + (numbers[i] + k))) {
					sum.add(numbers[i] + (numbers[i] + k));
					count++;
				}
			}
		}
		System.out.println("\nCount: " + count);
	}

		
}


