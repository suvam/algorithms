// URLify a string


package cci.ch1;
import java.io.*;

public class URLify {

	public static void urlify(char[] s, int truelength) {
		int tail = s.length - 1;
		int string_tail = truelength - 1;

		while(string_tail > -1) {
			if(s[string_tail] == ' ') {	// white space	
				s[tail] = '0';
				tail--;
				s[tail] = '2';
				tail--;
				s[tail] = '%';
				tail--;
				string_tail--;
			}
			else {
				s[tail] = s[string_tail];
				tail--;
				string_tail--;
			}
		}
	}

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter string: ");
		char[] s_chars = br.readLine().trim().toCharArray();

		int spaces = 0;

		for(int i = 0; i<s_chars.length; i++){
			if(s_chars[i] == ' ')
				spaces++;
		}

		char[] augmented_s_chars = new char[s_chars.length + (2*spaces)];

		for(int i=0; i<s_chars.length; i++) {
			augmented_s_chars[i] = s_chars[i];
		}

		urlify(augmented_s_chars, s_chars.length);
		for(int i=0; i<augmented_s_chars.length; i++) {
			System.out.print(augmented_s_chars[i]);
		}
	}
}
