// Check whether one string is a permutation of the other
// Suvam Mukherjee

package cci.ch1;

import java.util.HashMap;

public class CheckPermutation {

	public static void computeCharCount(HashMap<Character, Integer> counts, String s) {
		char[] s_chars = s.toCharArray();

		for(int i=0; i<s_chars.length; i++) {
			if(!(counts.containsKey(s_chars[i]))) {
				counts.put(s_chars[i], 1);
			}
			else 
				counts.put(s_chars[i], counts.get(s_chars[i])+1);
		}
	}

	public static boolean checkPermute(String s1, String s2) {
		if(s1.length() != s2.length())
			return false;

		HashMap<Character, Integer> s1_count = new HashMap<Character, Integer>();
		HashMap<Character, Integer> s2_count = new HashMap<Character, Integer>();
		computeCharCount(s1_count, s1);
		computeCharCount(s2_count, s2);
		
		for(char c: s1_count.keySet()) {
			if(s1_count.get(c) != s2_count.get(c))
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		String s1 = args[0].trim();
		String s2 = args[1].trim();

		System.out.println(checkPermute(s1, s2));
	}
}
			
