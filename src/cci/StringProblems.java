/**
 * Given two strings, print if one is a permutation of the other. 
 * Optimal running time: BCR = O(n)
 */
package cci;

import java.util.*;
import java.io.*;

/**
 * @author suvam
 *
 */
public class StringProblems {

	/**
	* Perform string compression 
	* aaabcc => a3bc2
	*/
	public static String compressString(String s) {
		char[] str = s.toCharArray();
		int scan = 0;
		int run = 1;
		int count = 1;
		StringBuilder outStr = new StringBuilder();

		while(scan < str.length) {
			char currSymb = str[scan];
			count = 1;
		
			while(run < str.length && (str[run] == currSymb)) {
				count++;
				run++;
			}

			if(count > 1){
				outStr.append(""+currSymb);
				outStr.append(""+count);
			}
			else
				outStr.append(""+currSymb);
			
			scan = run;
			run = run + 1;					
		}
	return outStr.toString();
	}

	/**
	* Count the frequency of each character in the input string
	**/
	public static void getCharCount(String s, HashMap<Character, Integer> charCount) {
		for(int i=0; i<s.length(); i++) {
			if(! Character.isWhitespace(s.charAt(i))) {
				if(charCount.get(s.charAt(i)) == null) {
					charCount.put(s.charAt(i), 1);
				}
				else {
					charCount.put(s.charAt(i), charCount.get(s.charAt(i)) + 1);
				}
			}
		}
	}

	/**
	 * Check if s1 is a permutation of a palindrome
	*/
	public static boolean isPermutationOfPalindrome(String s1) {
		boolean foundOddRun = false;
		boolean oddLength = false;
	
		if(s1.length() % 2 != 0)
			oddLength = true;
		
		HashMap<Character, Integer> charCount = new HashMap<Character, Integer>();
		getCharCount(s1, charCount);

		for(char c: charCount.keySet()) {
			if((charCount.get(c) % 2 != 0) && !oddLength)
				return false;
			if((charCount.get(c) %2 !=0) && !foundOddRun) {
				foundOddRun = true;	
				continue;
			}
			if((charCount.get(c) %2 != 0) && foundOddRun)
				return false;
		}
		return true;
	}

	/**
	 * Checks if s1 and s2 are permutations of each other.
	 * @param s1
	 * @param s2
	 * @return true if s1 and s2 are permutations of each other, false otherwise
	 */
	public static boolean isPermutation(String s1, String s2) {
		HashMap<Character, Integer> countS1 = new HashMap<Character, Integer>();
		HashMap<Character, Integer> countS2 = new HashMap<Character, Integer>();

		if(s1.length() != s2.length())
			return false;

		for(int i =0; i<s1.length(); i++) {
			if(countS1.get(s1.charAt(i)) == null)	{
				countS1.put(s1.charAt(i), 1);
			}
			else {
				countS1.put(s1.charAt(i), countS1.get(s1.charAt(i)) + 1);
			}
		}

		for(int i =0; i<s2.length(); i++) {
                        if(countS2.get(s2.charAt(i)) == null)   {
                                countS2.put(s2.charAt(i), 1);
                        }
                        else {
                                countS2.put(s2.charAt(i), countS2.get(s2.charAt(i)) + 1);
                        }
                }

		// Compare 
		for(char c: countS1.keySet()) {
			if(countS2.get(c) == null) 
				return false;

			if(countS1.get(c) != countS2.get(c)) 
				return false;
		}

		return true;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("\nEnter string: ");
		String s0 = br.readLine();
		System.out.println(compressString(s0));
		System.out.println("\nIs palindrome: " + isPermutationOfPalindrome(s0));		
		System.out.println("\nEnter string 1: ");
		String s1 = br.readLine();
		System.out.println("\nEnter string 2: ");
		String s2 = br.readLine();
		System.out.println("\nPermutation: " + isPermutation(s1, s2));
	}

}
