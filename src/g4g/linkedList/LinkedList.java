/**
 * 
 */
package g4g.linkedList;

import java.util.Random;

/**
 * @author suvam
 *
 */
public class LinkedList {
	
	private Node head;
	private int size;
	
	public LinkedList() {
		head = null;
		size = 0;
	}

	/**
	 * @param numNodes : Number of nodes in the linked list
	 */
	public int createList(int numNodes) {
		
		// Create list with numNodes nodes
		head = null;
		size = numNodes;
		Random rand = new Random();
		
		for(int i=0; i<numNodes; i++) {
			Node v = new Node(rand.nextInt(100), null);
			
			v.setNext(head);
			head = v;
		}
		
		return 1;
	}
	
	public void printList() {
		
		Node temp = head;
		
		if(temp == null) {
			System.out.println("\nNothing to print. Empty list.");
			return;
		}
		System.out.println("\n");
		while(temp!=null) {
			System.out.print("[ " + temp.getData() + " ] --> ");
			temp = temp.getNext();
		}
		System.out.print("null");
	}
	
	public void insertAtFront(int data) {
		Node n = new Node(data, null);
		
		n.setNext(head);
		head = n;
		size++;
	}
	
	public void insertAtEnd(int data) {
		Node n = new Node(data, null);
		
		if(size == 0) { 	// Empty List
			n.setNext(head);
			head = n;
			size++;
		}
		else {
			Node temp = head;
			
			while(temp.getNext() != null) {
				temp = temp.getNext();
			}
			
			temp.setNext(n);
			size++;
		}
	}
	
	/**
	 * Insert a node after a given node
	 * @param n: Insert after node n in the list
	 * @param data: Data for the new node
	 */
	public void insertAtPosition(Node n, int data) {
		Node tempN = new Node(data, null);
		
		if(size == 0) {
			System.out.println("\nEmpty List. Cannot process.");
			return;
		}
		
		Node temp = head;
		
		// Traverse the list until the node is found
		while(! temp.isEquals(n)) {
			temp = temp.getNext();
		}
		tempN.setNext(temp.getNext());
		temp.setNext(tempN);
		size++;
	}
	
	/**
	 * Return the current number of elements in the list
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Return a pointer to a random node in the list.
	 * Warning: Returns null if the list is empty.
	 */
	public Node getRandomNodeInList() {
		Random rand = new Random();
		
		if(size == 0) {
			System.err.println("\nEmpty List. Unable to sample.");
			return null;
		}
		int index = rand.nextInt(size);
		
		if(index == 0)
			return head;
		
		Node temp = head;
		for(int i=1; i<=index; i++) {
			temp = temp.getNext();
		}
		
		return temp;
	}
	
	/**
	 * Return and remove the first element.
	 * Warning: Returns null if the list is empty.
	 */
	public Node returnAndRemoveFirst() {
		
		if(size == 0) {
			System.out.println("\nNothing to delete. Empty list.");
			return null;
		}
		
		Node headNode = new Node(head);
		head = head.getNext();
		size--;
		System.gc();
		return headNode;
	}
	
	/**
	 * Swap two nodes in the linked list by rearranging pointers.
	 * Based on code from GeeksForGeeks.
	 * @param x: Node 1 to be swapped
	 * @param y: Node 2 to be swapper
	 * TODO
	 */
	public void swapNodes(Node x, Node y) {
		if(x.isEquals(y)) { 	// Equal nodes, nothing to swap
			return;
		}
		
		// Scan for x, and store the prevX
		Node prevX = null;
		Node currX = this.head;
		while(!(currX == null) && !(currX.isEquals(x))) {
			prevX = currX;
			currX = currX.getNext();
		}
		
		// Scan for y, and store the prevY
		Node prevY = null;
		Node currY = this.head;
		while(!(currY == null) && !(currY.isEquals(currY))) {
			prevY = currY;
			currY = currY.getNext();
		}
		
		// If either of x or y doesn't exist, exit
		if((currX == null) || (currY == null)) {
			return;
		}
		
		// Alter the next pointer of prevX
		if(prevX != null) {		// x is not the head
			prevX.setNext(currY);
		}
		else {
			head = currY;
		}
		
		// Alter the next pointer of prevY
		if(prevY != null) {		// y is not the head
			prevY.setNext(currX);
		}
		else {
			head = currX;
		}
		
		// Update the next pointers of x and y
		Node temp = currX.getNext();
		currX.setNext(currY.getNext());
		currY.setNext(temp);
	}
}
 