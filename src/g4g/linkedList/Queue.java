/**
 * 
 */
package g4g.linkedList;

/**
 * @author suvam
 *
 */
public class Queue {
	
	private LinkedList elements;
	
	public Queue() {
		elements = new LinkedList();
	}
	
	/**
	 * Enqueue an element in the queue: insert the element to the end of the list
	 * @param data: to be enqueued
	 */
	public void enqueue(int data) {
		elements.insertAtEnd(data);
	}
	
	/**
	 * Dequeue the queue
	 * @return the data at the start of the queue
	 */
	public Node dequeue() {
		return elements.returnAndRemoveFirst();
	}
	
	/**
	 * Print the queue out
	 */
	public void printQ() {
		elements.printList();
	}
	

}
