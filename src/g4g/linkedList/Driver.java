/**
 * 
 */
package g4g.linkedList;

/**
 * @author suvam
 *
 */
public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList ll = new LinkedList();
		
		ll.createList(3);
		ll.printList();
		
		// Insert a node at the start
		ll.insertAtFront(13);
		ll.printList();
		System.out.println("\n" + ll.size());	// query size
		
		// Insert a node at the end
		ll.insertAtEnd(14);
		ll.printList();
		System.out.println("\n" + ll.size());	// query size
		
		// Insert after a random node
		Node n = ll.getRandomNodeInList();
		ll.insertAtPosition(n, 55);
		ll.printList();
		System.out.println("\n" + ll.size()); 	// query size
		
		// Test swapping of elements
		System.out.println("\nTesting List Swap");
		System.out.println("Initial list: ");
		ll.printList();
		Node n1 = ll.getRandomNodeInList();
		Node n2 = ll.getRandomNodeInList();
		ll.swapNodes(n1, n2);
		System.out.println("\nFinal list: ");
		ll.printList();
		
		// Testing Queue implementation
		Queue q = new Queue();
		q.enqueue(10);
		q.enqueue(20);
		q.enqueue(30);
		q.printQ();
		System.out.println(q.dequeue());
		q.printQ();
		
	}

}
