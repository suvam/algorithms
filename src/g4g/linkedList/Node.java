/**
 * 
 */
package g4g.linkedList;

/**
 * @author suvam
 *
 */
public class Node {
	
	private int data;
	private Node next;
	
	public Node(int data, Node next) {
		this.data = data;
		this.next = next;
	}
	
	public Node(Node n) {
		this.data = n.getData();
		this.next = null;
	}
	
	public int getData() {
		return this.data;
	}
	
	public void setData(int data) {
		this.data = data;
	}
	
	public Node getNext() {
		return this.next;
	}
	
	public void setNext(Node n) {
		this.next = n;
	}
	
	/**
	 * Argument node n equals this node if their data components are the same.
	 * Re-implement this to suit your needs.
	 * @param n: Node for comparison
	 * @return
	 */
	public boolean isEquals(Node n) {
		if(this.data == n.getData())
			return true;
		else return false;
	}
	
	@Override
	/**
	 * The toString method prints out the data component
	 */
	public String toString() {
		return "\nData: " + this.data;
	}

}
