
public class B {

	int x;
	int y;
	
	public B(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		B b1 = new B(5, 6);
		System.out.println(b1.x + ", " + b1.y);
		B b2 = new B(3,4);
		b1 = b2;
		System.out.println(b1.x + ", " + b1.y);
	}

}
