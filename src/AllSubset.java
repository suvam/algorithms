/*
* Print all subsets of a set
*/

public class AllSubset {

	public static void main(String[] args) {
		int  n = Integer.parseInt(args[0]);	// length of set
		String[] set = new String[n];

		for(int i=0; i<n; i++) {
			set[i] = args[i+1];
		}

		// There are 2^n possible subsets, and 2^n is simply 1<<n
		int numSubsets = 1<<n;

		for(int i=0; i<numSubsets; i++) {

			System.out.print("{");
			// each character in set has a unique index
			// 1<<j makes a binary string where only the j-th index is 1
			// i & (1<<j) indicates whether the j-th entry is present in the i-th subset
			for(int j=0; j<set.length; j++) {
				if((i & (1<<j)) > 0)
					System.out.print(set[j]);
			}
			System.out.print("}\n");
		}
	}
}

