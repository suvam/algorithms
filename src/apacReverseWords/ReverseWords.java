/**
 * Source: Google APAC Practice
 */
package apacReverseWords;

/**
 * @author suvam
 *
 */
import java.io.*;
import java.util.StringTokenizer;
public class ReverseWords {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("\nEnter input file path: ");
		String inPath = br.readLine();
		BufferedReader in = new BufferedReader(new FileReader(new File(inPath.trim())));
		
		System.out.println("\nOutfile path: ");
		String outPath = br.readLine();
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outPath.trim())));
		
		int numTests = Integer.parseInt(in.readLine());
		
		for(int i=1; i<=numTests; i++)
		{
			String s = in.readLine();
			
			StringTokenizer st = new StringTokenizer(s, " ");
			int numTokens = st.countTokens();
			String[] tokens = new String[numTokens];
			int j = 0;
			
			while(st.hasMoreTokens())
			{
				tokens[j] = st.nextToken();
				j++;
			}
			System.out.println("Case #" + i + ": ");
			out.write("Case #" + i + ": ");
			System.out.println("\nNext");
			for(j = tokens.length-1; j >=0; j--)
			{
				System.out.println("Case #" + i + ": ");
				out.write(tokens[j] + " ");
			}
			out.write("\n");
		}
		br.close();
		in.close();
		out.close();
		

	}

}
