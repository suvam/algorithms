// Input: A sorted array of n elements, with all elements distinct
// Output: Magic index i with A[i] = i, if it exists
// Run-time= O(log n)
// Suvam Mukherjee, 2017

public class MagicIndex {

	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);	// num elements
		int[] M = new int[n];
		for(int i=0; i<n; i++) {
			M[i] = Integer.parseInt(args[i+1]);
		}

		int index = computeIndex(M, 0, n-1);
		if(index == -1)
			System.out.println("No magic index found");
		else
			System.out.println("Magic index: " + index);
	}

	public static int computeIndex(int[] M, int start, int stop) {
		if(start == stop && M[start]!=M[stop])
			return -1;

		int mid = start + ((stop - start) / 2);

		if(M[mid] == mid)
			return mid;

		if(M[mid] < mid)
			return computeIndex(M, mid+1, stop);

		if(M[mid] > mid)
			return computeIndex(M, start, mid-1);
		return -1;
	}

}
