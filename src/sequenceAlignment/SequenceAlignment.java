/**
 * Given two strings over alphabet A, X and Y, align X and Y to minimize the cost.
 * 
 * Inputs:
 * A file input.txt containing the following lines: 
 * 1. Alphabet size (n)
 * 2. Line containing the list of alphabet
 * 3. n^2 Lines containing n^2 numbers, each denoting the mismatch penalty for ab
 * 4. Gap penalty
 * 5. Input String X
 * 6. Input String Y
 */
package sequenceAlignment;

import java.io.*;
import java.lang.*;
import java.util.*;
/**
 * @author suvam
 *
 */
public class SequenceAlignment {
	
	/*
	 * Find the index of the characters a and b.
	 * Return the appropriate penalty from the penalty table
	 */
	public static int computeMismatchPenalty(char a, char b, int[][] penaltyTable, char[] letter)
	{
		int indexOfa = 0, indexOfb = 0;
		
		for(int i=0; i<letter.length; i++)
			if(letter[i] == a)
			{
				indexOfa = i;
				break;
			}
		
		for(int i=0; i<letter.length; i++)
			if(letter[i] == b)
			{
				indexOfb = i;
				break;
			}
		
		return penaltyTable[indexOfa][indexOfb];
	}
	
	public static int min(int a, int b, int c)
	{
		int min;
		
		if(a <= b)
			min = a;
		else
			min = b;
		
		if(min < c)
			min = c;
		
		return min;
			
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		// Process the inputs to set-up data structures
		String path;
		System.out.println("\nInput path to input file: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		path = br.readLine().trim();
		br = new BufferedReader(new FileReader(new File(path)));
		
		int alphSize = Integer.parseInt(br.readLine());	// Line 1 is alphabet size
		String alphabet = br.readLine();	// Line 2 is alphabet
		
		// Process alphabet
		StringTokenizer st = new StringTokenizer(alphabet);
		char[] letter = new char[alphSize];
		
		int i = 0;
		while(st.hasMoreTokens())
		{
			letter[i] = st.nextToken().charAt(0);
		}
		
		// Process the mismatch penalties
		int[][] mismatchPenalty = new int[alphSize][alphSize];
		
		for(i=0; i<alphSize; i++)
			for(int j=0; j<alphSize; j++)
				mismatchPenalty[i][j] = Integer.parseInt(br.readLine().trim());
		
		int gapPenalty = Integer.parseInt(br.readLine().trim());	// Process gap penalty
		
		char[] X = br.readLine().toCharArray();	// Process input string X
		char[] Y = br.readLine().toCharArray();	// Process input string Y
		
		// Processing Input finished
		
		int xLength = X.length;
		int yLength = Y.length;
		
		int[][] M = new int[xLength+1][yLength+1];
		
		M[0][0] = 0;
		
		for(i=1; i<=xLength; i++)
			M[i][0] = i * gapPenalty;
		
		for(int j=1; j<=yLength; j++)
			M[0][j] = j * gapPenalty;
		
		for(i=1; i<=xLength; i++)
		{
			for(int j=1; j<=yLength; j++)
			{
				M[i][j] = min(M[i-1][j-1] + computeMismatchPenalty(X[i], Y[j], mismatchPenalty, letter), gapPenalty + M[i-1][j], gapPenalty + M[i][j-1]);
			}
		}
		
		System.out.println("Minimum Penalty: " + M[xLength][yLength]);
			
		br.close();
	}

}
