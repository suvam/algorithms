package matrixTraversal;

/*
 * Input: M[n][n]
 * Output: Number of ways to travel from any point (x,y) to (n,n)
 * 
 * args[0]: n, size of matrix
 * args[1]: x
 * args[2]: y
 * 
 * @author Suvam Mukherjee
 */

public class Driver {
	
	private static int M[][];
	
	public static void main(String[] args)
	{
		if(args.length!=3)
		{
			System.out.println("\nInvalid number of arguments");
			System.exit(1);
		}
		
		int limit = Integer.parseInt(args[0]);
		
		int xPos = Integer.parseInt(args[1]);
		int yPos = Integer.parseInt(args[2]);
		
		
		M = new int[limit+1][limit+1];
		
		M[limit][limit] = 1;
		//System.out.println("M[" + limit + "][" + limit + "]: " + M[limit][limit]);
		
		for(int j=limit-1; j>=1; j--)
		{
			//System.out.println(j);
			M[limit][j] = 1;
			//System.out.println("M[" + limit + "][" + j + "]: " + M[limit][j]);
		}
		
		for(int i=limit-1; i>=1; i--)
		{
			M[i][limit] = 1;
			//System.out.println("M[" + i + "][" + limit + "]: " + M[i][limit]);
		}
			
		
		for(int i = limit-1; i>=1; i--)
		{
			for(int j = limit-1; j>=1; j--)
			{
				M[i][j] = M[i+1][j] + M[i][j+1];
			}
		}
		
		System.out.println("Total number of ways to reach [n,n] from [" + xPos + "," + yPos + "]: " + M[xPos][yPos]);
	}

}
