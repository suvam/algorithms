/*
* Robot in a maze. Given r X c matrix, go from (0,0) to (r-1, c-1).
* Certain cells are out of bound.
*/
import java.io.*;
import java.util.*;

class Cell {
	private int x;
	private int y;
	
	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setCoord(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	@Override
	public boolean equals(Object c) {
		System.out.println("\nEquals invoked");
		if(((Cell)c).getX()==this.x && ((Cell) c).getY()==this.y)
			return true;
		return false;
	}
}

public class RobotMaze {

public static int min(int x, int y) {
	if (x < y)
		return x;
	else
		return y;
}

public static void main(String[] args) throws IOException {
	HashSet<Cell> invalid = new HashSet<Cell>();
	int[][] M;
	int r = Integer.parseInt(args[0]);
	int c = Integer.parseInt(args[1]);
	M = new int[r][c];
	int max = (r+c+1);

	int n = Integer.parseInt(args[2]);
	int i = 1;
	while(n != 0) {
		int x = Integer.parseInt(args[2+i]);
		int y = Integer.parseInt(args[2+ (i+1)]);
		Cell cl = new Cell(x, y);
		invalid.add(cl);
		i += 2;
		n -= 1;
	}
		
	M[r-1][c-1] = 0;	// This is the destination
	
	// Handle the bottom row
	for(int j=c-2; j>=0; j--) {
		M[r-1][j] = 1 + M[r-1][j+1];
	}

	// Handle right column
	for(int j=r-2; j>=0; j--) {
		M[j][c-1] = 1 + M[j+1][c-1];
	}

	// For each invalid cell, make the path length max
	for(Cell cl: invalid) {
		M[cl.getX()][cl.getY()] = max;
	}

	// Compute min path for the rest
	for(int j=r-2; j>=0; j--) {
		for(int k=c-2; k>=0; k--) {
			// Check if we have a valid cell
//			Object cl = new Cell(j,k);
//			if(invalid.contains(cl))
//				continue;
			if(M[j][k] == max)
				continue;
	
			M[j][k] = min(1+M[j+1][k], 1+M[j][k+1]);
		}
	}

	// Print memoized solution
	for(int j=0; j<r; j++) {
		for(int k=0; k<c; k++) {
			System.out.print(M[j][k] + "   ");
		}
		System.out.println("");
	}		
	}
}

