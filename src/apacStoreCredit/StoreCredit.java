/**
 *  APAC Practice problem to find two items which have combined worth equal to given credit.
 */
package apacStoreCredit;

import java.io.*;
import java.util.StringTokenizer;

/**
 * @author suvam
 *
 */
public class StoreCredit {
	
	public static void computeStoreCredit(int credit, Data[] d, BufferedWriter out, int id) throws IOException
	{
		int done = 0;
		int index1 = -1, index2 = -1;
		
		for(int i=0; i<d.length && done != 1; i++)
		{
			index1 = i;
			// Search if credit - current exists
			for(int j=0; j<d.length && j != i; j++)
			{
				if(d[j].getData() == (credit - d[i].getData()))
				{
					index2 = j;
					done = 1;
					break;
				}
			}	
		}
		index2++;
		index1++;
		out.write("Case #"+id+": " + index2 + " " + index1 + "\n");
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String inPath;
		
		System.out.println("\nEnter path to input file: ");
		inPath = br.readLine();
		
		BufferedReader in = new BufferedReader(new FileReader(new File(inPath.trim())));
		
		System.out.println("\nEnter path to output file: ");
		String outPath = br.readLine();
		
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outPath.trim())));
		
		int numOfTests = Integer.parseInt(in.readLine());
		
		for(int i=1; i<=numOfTests; i++)
		{
			System.out.println("\nAnalyzing case: " + i);
			int credit = Integer.parseInt(in.readLine());
			int numItems = Integer.parseInt(in.readLine());
			
			// Create an array of Data
			Data[] d = new Data[numItems];
			
			for(int j = 0; j<d.length; j++)
				d[j] = new Data();
			
			// Parse the prices of each item
			String prices = in.readLine();
			//System.out.println("\nPrices: " + prices);
			StringTokenizer st = new StringTokenizer(prices, " ");
			
			int j = 0;
			int numOfTokens = st.countTokens();
			//System.out.println("\nTokens: " + numOfTokens);
			while(st.hasMoreTokens())
			{
				//System.out.println("\nIteration");
				d[j].setData(Integer.parseInt(st.nextToken()));
				d[j].setIndex(j);
				//System.out.println("\nd[" + j + "] set to " + d[j].getData());
				j++;
			}
			computeStoreCredit(credit, d, out, i);
		}
		
		br.close();
		in.close();
		out.close();

	}

}
