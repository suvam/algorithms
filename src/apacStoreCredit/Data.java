/**
 * Store the data and the index
 */
package apacStoreCredit;

/**
 * @author suvam
 *
 */
public class Data {
	private int data;
	private int index;
	
	public void setData(int data)
	{
		this.data = data;
	}
	
	public void setIndex(int index)
	{
		this.index = index;
	}
	
	public int getData()
	{
		return data;
	}
	
	public int getIndex()
	{
		return index;
	}

}
