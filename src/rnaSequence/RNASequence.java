/**
 * Given a string of bases, B = b1, b2, ..., bN,
 * generate the secondary structure with optimum free energy (maximum base pairings).
 * 
 * Bases are A,C,G,U
 * A pairs with U, C pairs with G
 * Minimal distance of 5 between pairing bases.
 * 
 * Input string must be args[0]
 */
package rnaSequence;

/**
 * @author suvam
 *
 */
public class RNASequence {
	
	/*
	 * Return the maximum of two numbers
	 */
	public static int max(int a, int b)
	{
		if(a<b)
			return b;
		else
			return a;
	}
	
	/*
	 * Recursive procedure to print the secondary structure
	 */
	public static void printSolution(int i, int j, int[][] M, char[] bases)
	{
		if(i >= j-4)
			return;
		else
		{
			if(M[i][j] == M[i][j-1])	// j is not part of the solution
				printSolution(i, j-1, M, bases);
			
			else
			{
				int t = i;
				for(; t < j-4; t++)
				{
					if(M[i][j] == 1 + M[i][t-1] + M[t+1][j-1])
						break;
				}
				System.out.println(t + "-->" + j + " === " + bases[t] + "-->" + bases[j]);
				printSolution(i, t-1, M, bases);
				printSolution(t+1, j-1, M, bases);
			}
		}
	}

	/**
	 * @param args[0]: Base string of the form b1.b2...bN
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if(args.length != 1)
		{
			System.out.println("\nInvalid number of arguments.");
			System.exit(1);
		}
		
		// Store in an array
		char[] input = (args[0].trim()).toCharArray();
		char[] bases = new char[input.length + 1];
		
		for(int i=0; i<input.length; i++)
			bases[i+1] = input[i];
		
		int len = bases.length - 1;
		//System.out.println("Bases length: " + len);
		int[][] M = new int[len+1][len+1];	// Memoization matrix
		
		// Initialization phase
		for(int j=0; j<=len-4; j++)
			for(int i=j-4; i<=len; i++)
				if(i<0)
					continue;
				else
				{
					M[i][j] = 0;
				}
		
		int j = 0;
		for(int k=5; k<=len-1; k++)	// Shorter intervals first
		{
			//System.out.println("\nIn first loop");
			for(int i=1; i<=len-k; i++)
			{
				//System.out.println("\nIn second loop");
				//System.out.println("\nCurrent base[i]: " + bases[i]);
				j = i+k;
				//System.out.println("\nj: " + j);
				//System.out.println(bases.length);
				//System.out.println("\nCurrent base[j]: " + bases[j]);
	
				// Find the maximum solution when j is part of the secondary structure
				int max_t = Integer.MIN_VALUE;
				for(int t=i; t<j-4; t++)
				{
					//System.out.println("\nCurrent t: " + t);
					//System.out.println("\nCurrent bases[t]: " + bases[t]);
					//System.out.println("\nCurrent bases[j]: " + bases[j]);
					// t must satisfy pairing constraints
					if(((bases[t]=='A' && bases[j]=='U') || (bases[t]=='U' && bases[j]=='A') ||
							(bases[t]=='C' && bases[j]=='G') || (bases[t]=='G' && bases[j]=='C')
							) 
					)
						max_t = max(max_t, 1+M[i][t-1]+M[t+1][j-1]);
						//System.out.println(max_t);
				}
				// Compare with the value if j is not part of the secondary structure
				M[i][j] = max(M[i][j-1], max_t);
				
			}
		}
		// Uncomment to print memoized matrix
//		for(int i=0; i<=len; i++)
//		{
//			for(j=0; j<=len; j++)
//				System.out.print(M[i][j] + " ");
//			System.out.println(" ");
//		}
		System.out.println("\nMaximum number of base pairings: " + M[1][len]);
		
		// Print out the secondary structure
		printSolution(1, len, M, bases);
		
	}
	

}
