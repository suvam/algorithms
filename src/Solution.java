import java.util.*;

public class Solution {
	private static HashMap<Integer, Integer> valToIndex = new HashMap<Integer, Integer>();
    
    public static int[] twoSum(int[] nums, int target) {
    
        int index;
         int index1 = -1, index2 = -1;
         
            // Set up map
        for(index = 0; index<nums.length; index++) {
            valToIndex.put(nums[index], index);
        }
        
        System.out.println(valToIndex);
        
       
        
        for(index = 0; index <nums.length; index++) {
            int currNumber = nums[index];
            int diff = target - currNumber;
            index1 = index;
            
            if(valToIndex.containsKey(diff) && !(valToIndex.get(diff) == index1)) {
                index2 = valToIndex.get(diff);
                break;
            }
                
        }
        
        if(index==nums.length) {
            int res[] = {0, 0};
            return res;
        }
        
        else {
            int res[] = {index1, index2};
            return res;
        }
    }
    
    public static void main(String[] args) {
    	int[] arr = {3,2,3,4, -5};
    	int target = -2;
    	int[] ans = twoSum(arr, target);
    	System.out.println(ans[0] + " , " + ans[1]);
    }
    
//    public int[] twoSum(int[] nums, int target) {
//        int currNumber = 0;
//        int index1 = -1;
//        int index2 = -1;
//        boolean found = false;
//        int index;
//        for(index = 0; index < nums.length && !found; index++) {
//            currNumber = nums[index];
//            index1 = index;
//            int diff = target - currNumber;
//            
//            for(int j=0; j<nums.length; j++ ) {
//                if(nums[j] == diff && j != index1) {
//                    found = true;
//                    index2 = j;
//                    break;
//                }
//            }
//        }
//        
//        if(index == nums.length) {
//            int[] res = {0, 0};
//            return res;
//        }
//        else {
//        int[] res = {index1, index2};
//        return res;
//        }
//    }
}