package eggPuzzle;

/*
 * Solve the egg puzzle: given e eggs, n floors, determine with the least number of trials the "critical floor"
 * 
 * OPT[e,n] = min(1<=j<=n) { 1 + max{OPT[e-1, j], OPT[e, n-j]} }
 */

/*
 * @author suvam
 */

public class SolveEggPuzzle {
	
	private static int max(int a, int b)
	{
		if(a>b)
			return a;
		else return b;
	}
	
	public static void main(String[] args)
	{
		if(args.length != 2)
		{
			System.out.println("\nIncorrect number of arguments");
			System.exit(1);
		}
		
		int eggs =Integer.parseInt(args[0]);
		int floors = Integer.parseInt(args[1]);
		
		int[][] M = new int[eggs+1][floors+1]; 	// memoized storage
		
		// for a single egg, no other option but to scan through sequentially from the lowest floor up
		// in the worst case, needs to check each floor
		for(int i=0; i<=floors; i++)
		{
			M[1][i] = i;
		}
		
		// If there are 0 floors, no trial needed. If there is 1 floor, 1 trial suffices.
		for(int i=1; i<=eggs; i++)
		{
			M[i][0] = 0;
			M[i][1] = 1;
		}
		
		int minFloor = 0;
		
		// OPT[e,n] = min(1<=j<=n) { 1 + max{OPT[e-1, j-1], OPT[e, n-j]} }
		for(int i=2; i<= eggs; i++)
		{
			for(int k = 2; k<=floors; k++)
			{
				M[i][k] = Integer.MAX_VALUE;
				for(int j=1; j<=k; j++)
				{
					int temp = 1 + max(M[i-1][j-1], M[i][k-j]);
					
					if(temp < M[i][k])
					{
						M[i][k] = temp;
						minFloor = j;
					}
					
				}
			}
		}
		
		
		System.out.println("\n" + minFloor);
		System.out.println(M[eggs][floors]);	
		
	}

}
